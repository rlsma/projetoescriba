require 'test_helper'

class WordingTest < ActiveSupport::TestCase
  test "Titulo muito grande" do
  		wording = wordings(:titulo_grande)
  		assert_not wording.save, "redacao salva com titulo grande"
  	end
  	test "redacao sem texto" do
  		wording = wordings(:sem_texto)
  		assert_not wording.save, "redacao salva sem texto"
  	end
end
