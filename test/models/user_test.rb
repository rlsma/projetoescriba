require 'test_helper'

class UserTest < ActiveSupport::TestCase
  	test "Usuario sem nome" do
  		user = users(:sem_nome)
  		assert_not user.save, "Usuario salvo sem nome"
  	end
  	test "Usuario sem sobrenome" do
  		user = users(:sem_sobrenome)
  		assert_not user.save, "Usuario salvo sem sobrenome"
  	end
  	test "Usuario sem email" do
  		user = users(:sem_email)
  		assert_not user.save, "Usuario salvo sem email"
  	end
  	test "Usuario sem senha" do
  		user = users(:sem_senha)
  		assert_not user.save, "Usuario salvo sem senha"
  	end
  	test "Usuario com email invalido" do
  		user = users(:email_invalido)
  		assert_not user.save, "Usuario salvo com email invalido"
  	end
  	test "Nome muito grande" do
  		user = users(:nome_grande)
  		assert_not user.save, "Usuario salvo com Nome muito grande"
  	end
  	test "Sobrenome muito grande" do
  		user = users(:sobrenome_grande)
  		assert_not user.save, "Usuario salvo com Sobrenome muito grande"
  	end
end
