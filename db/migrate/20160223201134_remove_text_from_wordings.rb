class RemoveTextFromWordings < ActiveRecord::Migration
  def change
    remove_column :wordings, :text, :string
  end
end
