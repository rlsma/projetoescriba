class User < ActiveRecord::Base
	has_many :wordings
	has_many :reviews
	validates :name,
    length: {maximum: 200,
    message:" não pode ter mais do que 200 caracteres"}, 
    presence: {message:"em branco"}
	validates :lastname,length: {maximum: 200,
    message:" não pode ter mais do que 200 caracteres"}, 
    presence: {message:"em branco"}
  validates :encrypted_password, presence: true
  validates :email, email: true
	HUMANIZED_LASTNAME = {
    	lastname: "Sobrenome"
  	}
  	HUMANIZED_NAME = {
    	name: "Nome"
  	}
  	HUMANIZED_PASS = {
    	password: "Senha"
  	}
  	def self.human_attribute_name(attr, options = {})
    	HUMANIZED_LASTNAME[attr.to_sym] || 
    	 	HUMANIZED_NAME[attr.to_sym] || 
    	 	HUMANIZED_PASS[attr.to_sym] || super
  	end
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
