class Wording < ActiveRecord::Base
  belongs_to :user
  has_many :reviews
  validates :title, length: {maximum: 200, message:" não pode ter mais do que 200 caracteres"}
  validates :text, presence: {message: "não pode ser em branco"}
  HUMANIZED_TEXT = {
      text: "O texto"
  }
  HUMANIZED_TITLE = {
      text: "O texto"
  }
  def self.human_attribute_name(attr, options = {})
      HUMANIZED_TEXT[attr.to_sym] || super
    end
  def score
    @score = 0;
    if self.reviews.count == 0
      @score = 0
    else
      self.reviews.each do |r|
        if(r.valid?)
          @score += r.rate
        end
      end
      @score /= self.reviews.count
    end 
  end
end
