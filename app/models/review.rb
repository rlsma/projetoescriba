class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :wording

  validates :rate, presence: true, inclusion: { in: 0..1000 }

  def writer
  	return self.user.email
  end
end
