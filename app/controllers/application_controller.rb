class ApplicationController < ActionController::Base
before_filter :authenticate_user!
def page_not_found
    render plain: "404 Not Found", status: 404
  end
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

end
