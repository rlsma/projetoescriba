class ReviewsController < ApplicationController
	def create
		@wording = Wording.find(params[:wording_id])
		@review = @wording.reviews.create(review_params)
		@review.user = current_user

		if @review.save
			redirect_to @wording
		else
    		flash[:notice] = "Comentário invalido"
    		render "wordings/show"
    	end
    	
	end
	def destroy
		@review = Review.find(params[:id])
		@review.destroy
		redirect_to :back
	end
	private
	def review_params
		params.require(:review).permit(:rate,:comment)
	end
end
