class WordingsController < ApplicationController
	def new 
		@wording = Wording.new
	end 
	def my
		@wordings = current_user.wordings
	end
	def create
		@wording = Wording.new(post_params)
		@wording.user = current_user
		if @wording.save
            redirect_to @wording
        else
            render 'new'
        end
	end
	def index
		@wordings = Wording.all
	end
	def show
		flash[:notice] = ""
		@wording = Wording.find(params[:id])
	end

	def destroy
		@wording = Wording.find(params[:id])
		@wording.reviews.destroy
		@wording.destroy
		redirect_to :back
	end
	private
	def post_params
		params.require(:wording).permit(:title,:text)
	end
end
