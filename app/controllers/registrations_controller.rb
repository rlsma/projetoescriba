class RegistrationsController < Devise::RegistrationsController
	






	before_filter :configure_devise_permitted_parameters, if: :devise_controller?

	  protected

	  def configure_devise_permitted_parameters
	    permitted_params = [:name, :lastname, :email, :password, :password_confirmation]

	    if params[:action] == 'update'
	      devise_parameter_sanitizer.for(:account_update) { 
	        |u| u.permit(permitted_params << :current_password)
	      }
	    elsif params[:action] == 'create'
	      devise_parameter_sanitizer.for(:sign_up) { 
	        |u| u.permit(permitted_params) 
	      }
	    end
	  end
end