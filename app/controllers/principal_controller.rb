class PrincipalController < ApplicationController
  def home
  	@bests_wordings = Wording.all.sort {|left, right| right.score <=> left.score}
  	@bests_wordings = @bests_wordings.take(3)
  	if !user_signed_in?
  		redirect_to user_session_path
  	end
  end
end
